import express, { Request, Response } from 'express';

const app = express();

app
    .route('/api/v1/:status')
    .get((req: Request, res: Response) => {

        const status = +req.params.status;
        return res.status(status).send({ status });
    });

app.listen(3000, () => {
    console.log('Server running!! 🚀');
});
